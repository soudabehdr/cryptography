import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Soudabeh on 3/24/2017.
 */
public class OutPutFileWriter {

    /**
     * writeFile in method matne ra gerfete va unra dar addresse marbote zakhire mikonad.
     * @param address addresse filei ke gharar ast dar aan zakhire konad ra be unvane vorodi migirad.
     * @param str  matne ra ke gharar ast dar file write konad ra az vorodi migirad.
     * @throws IOException exception vorodi khoroji tolid shode ra be method balayi pas midahad.
     */
    public void writeFile(String address,String str )throws IOException{

        File file= new File(address);
        if(file.exists()) {
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            for (int i = 0; i < str.length(); i++) {

                bufferedOutputStream.write(str.charAt(i));

            }
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
        }
        else {
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            for (int i = 0; i < str.length(); i++) {

                bufferedOutputStream.write(str.charAt(i));

            }
            bufferedOutputStream.flush();
            bufferedOutputStream.close();

        }

    }


}
