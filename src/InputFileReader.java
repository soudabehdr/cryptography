import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Soudabeh on 3/24/2017.
 */
public class InputFileReader {

    /**
     * readFile yek matne ra az file be soorate String mikhand va unra be soorate bytei mikhanad va dar arayei az byte
     * zakhire mikonad.
     * @param address addresse file ra be unvane vorodi gerefte va un file ra mikhanad.
     * @return file khande shode ra dar arayei az byte zakhire karde va barmigardanad.
     * @throws IOException exception IO tolid shode ra be method balayi pas idahad.
     */
 public static  byte[] readFile(String address) throws IOException {

     File file = new File(address);

         long length = file.length(); // toole file ro b Byte barmigardoone
         byte[] bytes = new byte[(int) length];
         FileInputStream fileInputStream = new FileInputStream(file);
         BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

         for (int i = 0; i < length; i++) {
             bytes[i] = (byte) bufferedInputStream.read();
         }

         bufferedInputStream.close();
         return bytes;


 }


}
