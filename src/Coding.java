import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;

import static sun.security.x509.CertificateAlgorithmId.ALGORITHM;





public class Coding {
    /**
     * method  encodeFile baraye code kardane yek matn hast ke matn ra az file be soorate bytei khande va unra be
     * bitstream tabdil mikonad va 6 bit 6 bit joda karde va natije ra be character tabdil mikonad
     *
     * @param bytes az class InputFilereader arayei az byte ha gerefte va un ra be bitstream tabdil mokonad.
     * @return natije ra ke tabdil be character shode ast ra be soorate arayei az character ha bar migardanad.
     * @throws IOException Exception marbot b Ivorodi khoroji tolid shode ra be method balatar pas midahad.
     */

    public char[] encodeFile(byte[] bytes) throws IOException {


        /**
         * in araye baraye gharar dadane netije maghadiri bad az tabdil be bitstream va 6 ta 6ta joda kardan hast.
         */
        ArrayList<Integer> arrayList = new ArrayList();

        /**
         * chon har byte 8 bit hast baraye joda kardane 6 ta 6ta dar akhar ya 2 ta ya 4 ta va ya hich momken ast ezafi
         * biyayad. pas baghimande tedade byte ha ra hesab mikonim ta har 3 halate momken ra piyade sazi konim. pas chon
         * har 3 khane byte 4 khane baraye 6 tayi ha mishavad baghimande ra bar 3 hesab mikonim  ta tamame halat ha ra
         * barresi konim.
         */

        if ((bytes.length) % 3 == 0) {

            for (int i = 0; i < bytes.length; i = i + 3) {
                int k=0;
                if(bytes[i]<0){
                    int nn=bytes[i]&0xff;
                     k = (byte) (nn >>> 2);
                }
                else {
                     k = (byte) (bytes[i] >>> 2);
                }
                arrayList.add(k & 0xff);

                int l=0;
                if(bytes[i]<0){
                    int na=bytes[i]&0xff;
                     l = (byte) (na << 6);
                    l = l & 0xff;
                }
                else {
                     l = (byte) (bytes[i] << 6);
                    l = l & 0xff;
                }
                int m=0;
               if(bytes[i + 1]<0){
                   int nb=bytes[i + 1]&0xff;
                    m = (byte) (nb >>> 2);
                   m = m & 0xff;
               }
                else {
                    m = (byte) (bytes[i + 1] >>> 2);
                   m = m & 0xff;
               }

                int r = (l | m);
                r = r & 0xff;
                r = (byte) (r >>> 2);
                arrayList.add(r & 0xff);

                int s=0;
                if(bytes[i + 1]<0){
                    int nt=bytes[i + 1]&0xff;
                     s = (byte) (nt<< 4);
                    s = s & 0xff;
                }
                else {
                     s = (byte) (bytes[i + 1] << 4);
                    s = s & 0xff;
                }
                int x=0;
               if(bytes[i + 2]<0){
                   int nc=bytes[i + 2]&0xff;
                    x = (byte) (nc >>> 4);
                   x = x & 0xff;
               }
                else {
                    x = (byte) (bytes[i + 2] >>> 4);
                   x = x & 0xff;
               }
                int p = s | x;
                p = p >> 2;
                arrayList.add(p & 0xff);//-------------------

                int q=0;
                if(bytes[i + 2]<0){
                    int nr=bytes[i + 2]&0xff;
                     q = (byte) (nr<< 2);
                    q = q & 0xff;
                    q = q >>> 2;
                    q = q & 0xff;
                }
                else {
                     q = (byte) (bytes[i + 2] << 2);
                    q = q & 0xff;
                    q = q >>> 2;
                    q = q & 0xff;
                }

                arrayList.add(q);

            }
        }


//----------------------------------------------------------------------------------------------------------------------
        if ((bytes.length) % 3 == 1) {

            if (bytes.length == 1) {

                int k=0;
                if(bytes[0]<0){
                    int uk=(bytes[0]&0xff);
                     k=(byte)(uk>>>2);
                }
                else {
                     k = (byte) (bytes[0] >>> 2);
                }
                arrayList.add(k & 0xff);

                int l=0;
                if(bytes[0]<0){
                    int tk=(bytes[0]&0xff);
                     l = (byte) (tk << 6);
                    l = l & 0xff;
                    l = l >>> 6;
                    l = l & 0xff;
                }
                else {
                     l = (byte) (bytes[0] << 6);
                    l = l & 0xff;
                    l = l >>> 6;
                    l = l & 0xff;
                }

                arrayList.add(l);


            } else {

                for (int i = 0; i < (bytes.length) - 1; i = i + 3) {
                    int ac=0;
                    if(bytes[i]<0){
                       int ad =(bytes[i]&0xff);
                         ac = (byte) (ad >>> 2);

                    }
                    else {
                         ac = (byte) (bytes[i] >>> 2);
                    }
                    arrayList.add(ac & 0xff);

                    int ae=0;
                    int l=0;
                    if(bytes[i]<0){
                         ae=(bytes[i]&0xff);
                         l = (byte) (ae << 6);
                        l = l & 0xff;

                    }
                    else {
                         l = (byte) (bytes[i] << 6);
                         l = l & 0xff;
                    }
                    int m=0;
                    if(bytes[i+1]<0){
                        int af=(bytes[i+1]&0xff);
                         m = (byte) (af >>> 2);
                        m = m & 0xff;
                    }
                    else {
                        m = (byte) (bytes[i + 1] >>> 2);
                        m = m & 0xff;
                    }

                    int r = (l | m);
                    r = r & 0xff;
                    r = (byte) (r >>> 2);
                    arrayList.add(r & 0xff);

                    int s=0;
                    if(bytes[i+1]<0){
                        int ag=bytes[i + 1]&0xff;
                         s = (byte) (ag << 4);
                        s = s & 0xff;
                    }
                    else {
                         s = (byte) (bytes[i + 1] << 4);
                        s = s & 0xff;
                    }


                    int x=0;
                    if(bytes[i+2]<0){
                        int ab=(bytes[i+2]&0xff);

                        ab=ab&0xff;
                        x = (byte) (ab>>> 4);
                        x=x&0xff;
                    }
                    else {
                         x = (byte) (bytes[i + 2] >>> 4);
                         x = x & 0xff;
                    }
                    int p = s | x;
                    p=p&0xff;
                    p = p >>> 2;
                    arrayList.add(p & 0xff);//-------------------

                    int q=0;
                    if (bytes[i+2]<0){
                        int ah=bytes[i+2]&0xff;
                         q = (byte) (ah << 2);
                        q = q & 0xff;
                        q = q >>> 2;
                        q = q & 0xff;
                    }
                    else {
                         q = (byte) (bytes[i + 2] << 2);
                        q = q & 0xff;
                        q = q >>> 2;
                        q = q & 0xff;
                    }

                    arrayList.add(q);//----------------------------

                }
                int aa=0;
                if(bytes[bytes.length-1]<0){
                   int ai=(byte)(bytes[bytes.length-1]&0xff);
                     aa = (byte) (ai >>> 2);
                    aa = aa & 0xff;

                }
                else {
                     aa = (byte) (bytes[bytes.length - 1] >>> 2);
                    aa = aa & 0xff;
                }

                arrayList.add(aa);

                int bb=0;
                if(bytes[bytes.length - 1]<0){
                     bb = (byte) (bytes[bytes.length - 1] << 6);
                    bb = bb & 0xff;
                    bb = bb >>> 6;
                    bb = bb & 0xff;
                }
                else {
                    bb = (byte) (bytes[bytes.length - 1] << 6);
                    bb = bb & 0xff;
                    bb = bb >>> 6;
                    bb = bb & 0xff;
                }

                arrayList.add(bb);


            }
        }

//**********************************************************************************************************************
        if ((bytes.length) % 3 == 2) {

            if (bytes.length == 2) {
                int k=0;
                if(bytes[0]<0){
                    int aa=(bytes[0]&0xff);
                     k = (byte) (aa >>> 2);
                }
                else {
                     k = (byte) (bytes[0] >>> 2);
                }
                arrayList.add(k & 0xff);

                int l=0;
                if(bytes[0]<0){
                    int aa=(bytes[0]&0xff);
                     l = (byte) (aa<< 6);
                    l = l & 0xff;
                }
                else {
                     l = (byte) (bytes[0] << 6);
                    l = l & 0xff;
                }

                int m=0;
                if(bytes[1]<0){
                    int ab=bytes[1]&0xff;
                     m = (byte) (ab >>> 2);
                    m = m & 0xff;
                }
                else {
                    m = (byte) (bytes[1] >>> 2);
                    m = m & 0xff;
                }

                int r = (l | m);
                r = r & 0xff;
                r = (byte) (r >>> 2);
                arrayList.add(r & 0xff);

                int s=0;
                int x=0;
                if(bytes[1]<0){
                    int ac=bytes[1]&0xff;
                     s = (byte) (ac << 4);
                    s = s & 0xff;
                     x = (byte) (ac>>> 4);
                    x = x & 0xff;
                }
                else {
                     s = (byte) (bytes[1] << 4);
                    s = s & 0xff;
                     x = (byte) (bytes[1] >>> 4);
                    x = x & 0xff;
                }
                int p = s | x;
                arrayList.add(p & 0xff);//-------------------


            } else {

                for (int i = 0; i < (bytes.length) - 2; i = i + 3) {
                    int k=0;
                    if(bytes[i]<0){
                        int ad=bytes[i]&0xff;
                         k = (byte) (ad >>> 2);
                    }
                    else {
                         k = (byte) (bytes[i] >>> 2);
                    }

                    arrayList.add(k & 0xff);

                    int l=0;
                    if(bytes[i]<0){
                        int ag=bytes[i]&0xff;
                         l = (byte) (ag << 6);
                        l = l & 0xff;
                    }
                    else {
                         l = (byte) (bytes[i] << 6);
                        l = l & 0xff;
                    }
                    int m=0;
                    if(bytes[i+1]<0){
                        int ah=bytes[i + 1]&0xff;
                         m = (byte) (ah >>> 2);
                        m = m & 0xff;
                    }
                    else {
                         m = (byte) (bytes[i + 1] >>> 2);
                        m = m & 0xff;
                    }
                    int r = (l | m);
                    r = r & 0xff;
                    r = (byte) (r >>> 2);
                    arrayList.add(r & 0xff);

                    int s=0;
                    if(bytes[i+1]<0){
                        int ai=bytes[i + 1]&0xff;
                         s = (byte) (ai << 4);
                        s = s & 0xff;
                    }
                    else {
                         s = (byte) (bytes[i + 1] << 4);
                        s = s & 0xff;
                    }

                    int x=0;
                    if(bytes[i+2]<0){
                        int bb=bytes[i + 2]&0xff;
                         x = (byte)(bb >>> 4);
                    }
                    else {
                         x = (byte)(bytes[i + 2] >>> 4);
                    }
                    x = x & 0xff;
                    int n =(s | x) ;
                    n=n&0xff;
                    n = (byte)(n >>> 2);
                    arrayList.add(n & 0xff);//-------------------
                    int q=0;
                   if(bytes[i+2]<0){
                       int ba=bytes[i + 2]&0xff;
                        q = (byte) (ba << 2);
                       q = q & 0xff;
                       q = q >>> 2;
                       q = q & 0xff;
                   }
                    else {
                        q = (byte) (bytes[i + 2] << 2);
                       q = q & 0xff;
                       q = q >>> 2;
                       q = q & 0xff;
                   }

                    arrayList.add(q);//----------------------------

                }

                int bd=0;
                if(bytes[bytes.length - 2]<0){
                    int bc=bytes[bytes.length - 2]&0xff;
                     bd = (byte) (bc >>> 2);
                    bd = bd & 0xff;
                }
                else {
                     bd = (byte) (bytes[bytes.length - 2] >>> 2);
                    bd = bd & 0xff;
                }

                arrayList.add(bd);
                int ca=0;
               if (bytes[bytes.length - 2]<0){
                   int bg=bytes[bytes.length - 2]&0xff;
                    ca = (byte) (bg << 6);
                   ca = ca & 0xff;
               }
                else {
                    ca = (byte) (bytes[bytes.length - 2] << 6);
                   ca = ca & 0xff;
               }
                int cc=0;
                if(bytes[bytes.length - 1]<0){
                    int bk=bytes[bytes.length - 1]&0xff;
                     cc = (byte) (bk >>> 2);
                    cc = cc % 0xff;
                }
                else {
                     cc = (byte) (bytes[bytes.length - 1] >>> 2);
                    cc = cc % 0xff;
                }

                int dd = ca | cc;
                dd=dd&0xff;
                dd = dd >>> 2;
                dd = dd & 0xff;
                arrayList.add(dd);

                int ee=0;
                if(bytes[bytes.length - 1]<0){
                    int ea=bytes[bytes.length - 1]&0xff;
                     ee = (byte) (ea << 4);
                    ee = ee & 0xff;
                    ee = ee >>> 4;
                    ee = ee & 0xff;
                }
                else {
                     ee = (byte) (bytes[bytes.length - 1] << 4);
                    ee = ee & 0xff;
                    ee = ee >>> 4;
                    ee = ee & 0xff;
                }
                arrayList.add(ee);


            }
        }

        /**
         * araye intToCharArrayList baraye rikhtane maghadir int tabdil shode be character moadeleshan ast.
         */
        char[] intToCharArrayList = new char[arrayList.size()];
        /**
         * araye array tamami 64 ta maghadir momken ra darad ke gharar ast 6bit haye tabdil shode ra ba in maghadir
         * moadel konim.
         */
        char[] array = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

        for (int i = 0; i < arrayList.size(); i++) {
            intToCharArrayList[i] = array[arrayList.get(i)];
        }

        System.out.println("arraylistOfint"+arrayList); // arayei k byte ha toosh gharar gereftan
        for(int u=0; u<intToCharArrayList.length;u++) {
            System.out.println("int to Char arrayList:" + intToCharArrayList[u]);
        }

        return intToCharArrayList; //******* in arraylist bood
    }


    /**
     * methode decodeFile baraye decode kardane yek file hast. yek matn ra az file mikhanad va character be character
     * khande va 6 tayi hara tabdil be bit haye 8 tayi mikonad va yek byte shode va tabdil be byte mikonad va dar akhar
     * dar file zakhire mikonad
     * @param convertFileToString yek matn ra az file gerefte ke string hast va unra decode mikonad.
     * @return dar akhar chon text asli ra bayad bargardanad pas yek arayei az character ha bar migardanad.
     * @throws IOException exception vorodi khoroji ra be method balatar pas midahad.
     */
    public char[] decodeFile(String convertFileToString) throws IOException{


        char[] arrayOfChar=new char[(int)convertFileToString.length()];//*******
        int[] arrayOfInt=new int[arrayOfChar.length];//****
        ArrayList<Integer> arrayList= new ArrayList();//***********
        for(int i=0; i<convertFileToString.length(); i++) {
            arrayOfChar[i]=convertFileToString.charAt(i);

        }
        /**
         * intAndChar hashMap yek hashMapi hast ke 64 ta maghadir ra darad va vaghti methos az file mikhanad meghdare
         * moadele har char ra az tarighe hashMap be dast miyarad va dar araye zakhire mikonad.
         */

        HashMap<Character,Integer> intAndChar = new HashMap<Character, Integer>();

        intAndChar.put('A',0);   intAndChar.put('B',1);      intAndChar.put('C',2);
        intAndChar.put('D',3);   intAndChar.put('E',4);      intAndChar.put('F',5);
        intAndChar.put('G',6);   intAndChar.put('H',7);      intAndChar.put('I',8);
        intAndChar.put('J',9);   intAndChar.put('K',10);      intAndChar.put('L',11);
        intAndChar.put('M',12);   intAndChar.put('N',13);      intAndChar.put('O',14);
        intAndChar.put('P',15);   intAndChar.put('Q',16);      intAndChar.put('R',17);
        intAndChar.put('S',18);   intAndChar.put('T',19);      intAndChar.put('U',20);
        intAndChar.put('V',21);   intAndChar.put('W',22);      intAndChar.put('X',23);
        intAndChar.put('Y',24);   intAndChar.put('Z',25);      intAndChar.put('a',26);
        intAndChar.put('b',27);   intAndChar.put('c',28);      intAndChar.put('d',29);
        intAndChar.put('e',30);   intAndChar.put('f',31);      intAndChar.put('g',32);
        intAndChar.put('h',33);   intAndChar.put('i',34);      intAndChar.put('j',35);
        intAndChar.put('k',36);   intAndChar.put('l',37);      intAndChar.put('m',38);
        intAndChar.put('n',39);   intAndChar.put('o',40);      intAndChar.put('p',41);
        intAndChar.put('q',42);   intAndChar.put('r',43);      intAndChar.put('s',44);
        intAndChar.put('t',45);   intAndChar.put('u',46);      intAndChar.put('v',47);
        intAndChar.put('w',48);   intAndChar.put('x',49);      intAndChar.put('y',50);
        intAndChar.put('z',51);   intAndChar.put('0',52);      intAndChar.put('1',53);
        intAndChar.put('2',54);   intAndChar.put('3',55);      intAndChar.put('4',56);
        intAndChar.put('5',57);   intAndChar.put('6',58);      intAndChar.put('7',59);
        intAndChar.put('8',60);   intAndChar.put('9',61);      intAndChar.put('+',62);
        intAndChar.put('/',63);

        char c;
        /**
         * arrayOfChar tamame character haye khande shode ra dar bar darad.
         * arrayOfBinary binary shode character ha ra dar bar darad ke haman az hashMap be dast miavarim.
         *
         */
        byte[] arrayOfBinary=new byte[arrayOfChar.length];
        for(int k=0 ; k<arrayOfChar.length ; k++){
            c=arrayOfChar[k];
            Integer z= intAndChar.get(c);
            arrayOfInt[k]=z;

            Byte t= (byte)arrayOfInt[k];
            arrayOfBinary[k] =t;

        }


        /**
         * inja chon gharar ast har 6 tayi ra be 8 tayi tabdil konim pas har 4 ta 6 tayi moadele 3 ta 8 tayi hast pas
         * baghimande tedade byte ha ra bar 4 hesab mikonim ta tamami halat baresi shavad.
         */
        if((arrayOfBinary.length%4)==0) {
            for (int h = 0; h < arrayOfBinary.length; h=h+4) {
                int m = (byte) (arrayOfBinary[h] << 2); // ----------------- in baraye halati hast ke arrayOfBinary.length %4=0
                m = m & 0xff;
                int v = (byte) (arrayOfBinary[h + 1] >>> 4);
                v = v & 0xff;
                int t = m | v;
                t = t & 0xff;
                arrayList.add(t); // in faghat int hast va byte nist ino havaset bashe
                int l = (byte) (arrayOfBinary[h + 1] << 4);
                l = l & 0xff;
                int n = (byte) (arrayOfBinary[h + 2] >>> 2);
                n = n & 0xff;
                int o = l | n;
                o = o & 0xff;
                arrayList.add(o);
                int b = (byte) (arrayOfBinary[h + 2] << 6);
                b = b & 0xff;
                int z = (byte) (arrayOfBinary[h + 3] << 2);
                z = z & 0xff;
                z = z >>> 2;
                z = z & 0xff;
                int y = b | z;
                arrayList.add(y);
            }
        }

        if((arrayOfBinary.length%4)==2){ // moadele %3==1 to encode
            if(arrayOfBinary.length ==2){ // halati ke faghat 2 ta khoone dashte bashe
                if(arrayOfInt[0]<0) {

                    arrayOfBinary[0]= (byte)(arrayOfInt[0] + 256);
                }
                int a=(byte)(arrayOfBinary[0] <<2);
                a=a&0xff;
                if(arrayOfInt[1]<0) {

                    arrayOfBinary[1]=(byte)(arrayOfInt[1] + 256);
                }
                int b=(byte)(arrayOfBinary[1] <<6);
                b=b&0xff;
                b=b>>>6;
                b=b&0xff;
                int d= a|b;
                d=d&0xff;
                arrayList.add(d);
            }
            else {

                for (int h = 0; h < arrayOfBinary.length -2; h=h+4) {
                    if(arrayOfInt[h]<0) {

                        arrayOfBinary[h]= (byte)(arrayOfInt[h] &0xff);
                    }
                    int m = (byte) (arrayOfBinary[h] << 2);
                    m = m & 0xff;
                    if(arrayOfInt[h+1]<0) {

                        arrayOfBinary[h+1]= (byte)(arrayOfInt[h+1] &0xff);
                    }
                    int v = (byte) (arrayOfInt[h + 1] >>> 4);
                    v = v & 0xff;
                    int t = m | v;
                    t = t & 0xff;
                    arrayList.add(t); // in faghat int hast va byte nist ino havaset bashe
                    if(arrayOfInt[h+2]<0) {

                        arrayOfBinary[h+2]=(byte)(arrayOfInt[h+2]&0xff);
                    }
                    int l = (byte) (arrayOfInt[h + 1] << 4);
                    l = l & 0xff;
                    int n = (byte) (arrayOfInt[h + 2] >>> 2);
                    n = n & 0xff;
                    int o = l | n;
                    o = o & 0xff;
                    arrayList.add(o);
                    int b = (byte) (arrayOfInt[h + 2] << 6);
                    b = b & 0xff;
                    if(arrayOfInt[h+3]<0) {

                        arrayOfInt[h+3]= arrayOfInt[0] &0xff;
                    }
                    int z = (byte) (arrayOfInt[h + 3] << 2);
                    z = z & 0xff;
                    z = z >>> 2;
                    z = z & 0xff;
                    int y = b | z;
                    y=y&0xff;
                    arrayList.add(y);
                }

                if(arrayOfInt[arrayOfBinary.length-2]<0) {

                    arrayOfBinary[arrayOfBinary.length-2]= (byte)(arrayOfInt[arrayOfBinary.length-2]&0xff);
                }
                int a=(byte)(arrayOfBinary[arrayOfBinary.length-2] <<2);// be tedade count ke shomord vase 2 ta khoone akhar
                a=a&0xff;
                if(arrayOfInt[arrayOfBinary.length-1]<0) {

                    arrayOfBinary[arrayOfBinary.length-1]= (byte)(arrayOfInt[arrayOfBinary.length-1]&0xff);
                }
                int b=(byte)(arrayOfBinary[arrayOfBinary.length-1]<<6); //khoone akhar k 2 bitesho
                // lazem darim vase mohkam kari 6 ta miyarim chap va mibarim rast k motmaen shim sefre.
                b=b&0xff;
                b=b>>>6;
                b=b&0xff;
                int d= a|b;
                d=d&0xff;
                arrayList.add(d);
            }
        }

        if((arrayOfBinary.length%4)==3){
            if((arrayOfBinary.length)==3){
                int a =arrayOfBinary[0]<<2;
                a=a&0xff;
                int b=(byte)(arrayOfBinary[1]<<2);
                b=b&0xff;
                b=b>>>6;
                b=b&0xff;
                int d=a|b;
                d=d&0xff;
                arrayList.add(d);
                int e=(byte)(arrayOfBinary[1]<<4);
                e=e&0xff;
                int f=(byte)(arrayOfBinary[2]<<4);
                f=f&0xff;
                f=f>>>4;
                f=f&0xff;
                int g=e|f;
                g=g&0xff;
                arrayList.add(g);
            }
            else{

                for (int h = 0; h < arrayOfBinary.length -3; h=h+4) {
                    int m = (byte) (arrayOfBinary[h] << 2);
                    m = m & 0xff;
                    int v = (byte) (arrayOfBinary[h + 1] >>> 4);
                    v = v & 0xff;
                    int t = m | v;
                    t = t & 0xff;
                    arrayList.add(t); // in faghat int hast va byte nist ino havaset bashe
                    int l = (byte) (arrayOfBinary[h + 1] << 4);
                    l = l & 0xff;
                    int n = (byte) (arrayOfBinary[h + 2] >>> 2);
                    n = n & 0xff;
                    int o = l | n;
                    o = o & 0xff;
                    arrayList.add(o);
                    int b = (byte) (arrayOfBinary[h + 2] << 6);
                    b = b & 0xff;
                    int z = (byte) (arrayOfBinary[h + 3] << 2);
                    z = z & 0xff;
                    z = z >>> 2;
                    z = z & 0xff;
                    int y = b | z;
                    y=y&0xff;
                    arrayList.add(y);
                }
                int a =arrayOfBinary[arrayOfBinary.length-3]<<2;
                a=a&0xff;
                int b=(byte)(arrayOfBinary[arrayOfBinary.length-2]<<2);
                b=b&0xff;
                b=b>>>6;
                b=b&0xff;
                int d=a|b;
                d=d&0xff;
                arrayList.add(d);
                int e=(byte)(arrayOfBinary[arrayOfBinary.length-2]<<4);
                e=e&0xff;
                int f=(byte)(arrayOfBinary[arrayOfBinary.length-1]<<4);
                f=f&0xff;
                f=f>>>4;
                f=f&0xff;
                int g=e|f;
                g=g&0xff;
                arrayList.add(g);
            }
        }


        /**
         * arrayListToArrayOfChar baraye zakhire nahayii maghadire dakhele arraylist ke bayad az byte tabdil be character
         * shavad va zakhire gardand.
         */
        char[] arrayListToArrayOfChar = new char[arrayList.size()];


        for(int a=0;a<arrayList.size();a++) {

            arrayListToArrayOfChar[a]= (char)(arrayList.get(a).intValue());

        }


        return arrayListToArrayOfChar;

    }

}