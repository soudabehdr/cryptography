import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Soudabeh on 3/25/2017.
 */

/**
 * in class main baraye in neveshte shod ke chon cmd man moshkel dasht va natavanestam az unja ejra begiram az tarighe
 * revale manteghi ejra barname code ra ejra kardam.
 */
public class Main {
    public static void main(String[] args) throws IOException {


        File file = new File("e:\\file.txt");
        Coding coding= new Coding();
//----------------------------------------------------------------------------------------------------------------------

        //Encryption Procedure:
/*
        InputFileReader inputFileReader= new InputFileReader(); //inputFile
        byte[] array= inputFileReader.readFile("e:\\file.txt"); // read File
        char[] array1=coding.encodeFile(array); // Encode
        String str= new String(array1);
       //  SimpleShiftCryptography simpleShiftCryptography= new SimpleShiftCryptography("3");
        ComplexShiftCryptography complexShiftCryptography= new ComplexShiftCryptography("POINT");
       // String enc = simpleShiftCryptography.encrypt(str); // simple encrypt
        String enc =complexShiftCryptography.encrypt(str); //complex encrypt
        char[] arrayforDecode=coding.decodeFile(enc); //decode
        for(int p=0 ;p<arrayforDecode.length;p++){
            System.out.println("arrayfordecode: " +arrayforDecode[p]);
        }
        OutPutFileWriter outPutFileWriter= new OutPutFileWriter();
        String str1=new String(arrayforDecode);
        outPutFileWriter.writeFile("e:\\file.txt",str1); //writeFile*/
//----------------------------------------------------------------------------------------------------------------------
        //Decryption Procedure:

       InputFileReader inputFileReaderDec= new InputFileReader(); //inputFile
        byte[] arrayDec= inputFileReaderDec.readFile("e:\\file.txt"); // read File
        char[] array1Dec=coding.encodeFile(arrayDec); // Encode
        String strDec= new String(array1Dec);
      //  SimpleShiftCryptography simpleShiftCryptographyDec= new SimpleShiftCryptography("3");
        ComplexShiftCryptography complexShiftCryptographyDec= new ComplexShiftCryptography("POINT");
       // String dec = simpleShiftCryptographyDec.decrypt(strDec); //simple decrypt
        String dec = complexShiftCryptographyDec.decrypt(strDec); //complex decrypt
        char[] arrayforDecodeDec=coding.decodeFile(dec); //decode
        OutPutFileWriter outPutFileWriterDec= new OutPutFileWriter();
        String str1Dec=new String(arrayforDecodeDec);
        outPutFileWriterDec.writeFile("e:\\file.txt",str1Dec); //writeFile


      /*  InputFileReader inputFileReader= new InputFileReader();
        byte[] bytes = inputFileReader.readFile("e:\\file.txt");
        String str = new String(bytes);
        ComplexShiftCryptography complexShiftCryptography= new ComplexShiftCryptography("POINT");
        String str1= complexShiftCryptography.encrypt(str);
        System.out.println(str1);
        String str2= complexShiftCryptography.decrypt(str1);
        System.out.println(str2);
*/
    }
}
