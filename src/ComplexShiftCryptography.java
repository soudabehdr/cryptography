/**
 * Created by Soudabeh on 3/24/2017.
 */
public class ComplexShiftCryptography extends BaseCryptography{


    /**
     * ComplexShiftCryptography in sazande baraye gereftane meghdar hast ke chon field keyVal dar class pedar hast
     * pas baraye inke vaghti az vorodi migirad ba ramzgozari sade motamayez shavad yek field String dar class pedar
     * gerefte va inja dar sazande set mikonim.
     * @param keyString gerefte va ba keyval mojod dar class set mikonad.
     */
    public ComplexShiftCryptography(String keyString){

        keyVal=keyString;
    }
    public ComplexShiftCryptography(){

    }

    /**
     * encrypt baraye ramzgizari pichide hast. yek text ra gerefte va bar asase keyVal ke yek string hast unra ramzgozari
     * mikonad intori ke character be character keyVal ra khande va harkodam dar alphabet har jaygahi dashte bashand
     * character mojod dar text ra be aan meghdar shift midahad.
     * @param plainText yek text ra migirad va un ra ramzgizari mikonad
     * @return natije matne ramzgozari shode ra be soorate string bar migardanad.
     */
    public  String encrypt(String plainText){

       // plainText=plainText.toUpperCase();
        /**
         * cipherText matne ramzgozari shode ra dar in motaghayer jam mikonim.
         */
        String cipherText="";
        /**
         * keyToChar in motaghayer baraye in hast ke keyVal ra character be character khande va inja zakhire konim.
         */
        char[] keyToChar=new char[keyVal.length()];
        int j=0;

        /**
         * plainTextDivideKey in motaghayer baraye in hast ke chandin bar keyVal ba text ma barresi mishavad.
         * plainTextRemainKey in motaghayer baraye inke bebinim ch mizane az text baghi mimanad va ba tamami keyVal
         * set nmishavad hast .
         */
        int plainTextDivideKey=plainText.length()/keyVal.length();
        int plainTextRemainKey=plainText.length()%keyVal.length();
        for(int k=0;k<plainTextDivideKey ;k++) {
            for (int i = 0; i < keyVal.length(); i++) {

                keyToChar[i] = keyVal.charAt(i);
                /**
                 * jaygahe har character dar araye keyToChar ra dar alphabet  miyabim va dar motaghayer keyPos
                 * gharar midahim.
                 */
                int keyPos = alphabet.indexOf(keyToChar[i]);
                /**
                 * jaygahe har character dar text dade shode ra dar alphabet  miyabim va dar motaghayer keyPos
                 * gharar midahim.
                 */
                int charPos = alphabet.indexOf(plainText.charAt(j));  //indexOf shomare un char ro to string mide
                if (charPos < keyPos) {

                    /**
                     * numberOfShift chon momken ast vaghti avale araye bashim majboor shavim baraye shift be entehaye
                     * araye beravim pas yek if migozarim va check mikonim. sepas in ekhtelafe charPos va keyPos ra ba 64
                     * jam karde chon shift be chap hast va inja gharar midahim.
                     */
                    int numberOfShift = 64 - keyPos + charPos;
                    /**
                     * jaygah ke maloom shod char moadel ra dar in motaghayyre gharar midahim.
                     */
                    char newPosition = alphabet.charAt(numberOfShift);
                    cipherText += newPosition;
                }
                if (charPos >= keyPos) {
                    int numberOfShift = charPos - keyPos;
                    char newPosition = alphabet.charAt(numberOfShift);
                    cipherText += newPosition;
                }
                j++;
            }
        }



        for (int i = 0; i < plainTextRemainKey; i++) {

            keyToChar[i] = keyVal.charAt(i);
            int keyPos = alphabet.indexOf(keyToChar[i]);
            int charPos = alphabet.indexOf(plainText.charAt(j));  //indexOf shomare un char ro to string mide
            if (charPos < keyPos) {
                int numberOfShift = 64 - keyPos + charPos;
                char newPosition = alphabet.charAt(numberOfShift);
                cipherText += newPosition;
            }
            if (charPos >= keyPos) {
                int numberOfShift = charPos - keyPos;
                char newPosition = alphabet.charAt(numberOfShift);
                cipherText += newPosition;
            }
            j++;
        }

        return cipherText;

    }

//----------------------------------------------------------------------------------------------------------------------

    /**
     * decrypt yek matn ra gerefte va un ra ramz goshayi mokonad. intor ke yek KeyVal az noe string migirad va in bar
     * be ezaye har char dar text va char motanazere un dar keyVal be rast shift midahad.
     * @param cipherText yek text ra migirad va un ra ramzgoshayi mikonad
     * @return matn ramzgoshayi shode ra be soorate String baz migardanad.
     */
    public  String decrypt(String cipherText){

      //  cipherText=cipherText.toUpperCase();
        String plainText="";
        char[] keyToChar=new char[keyVal.length()];
        int j=0;

        int cipherTextDivideKey=cipherText.length()/keyVal.length();
        int cipherTextRemainKey=cipherText.length()%keyVal.length();
        for(int k=0;k<cipherTextDivideKey ;k++) {
            for (int i = 0; i < keyVal.length(); i++) {
                keyToChar[i] = keyVal.charAt(i);
                int keyPos = alphabet.indexOf(keyToChar[i]);
                int charPos = alphabet.indexOf(cipherText.charAt(j));  //indexOf shomare un char ro to string mide
                int numberOfShift = (keyPos + charPos) % 64;
                char newPosition = alphabet.charAt(numberOfShift);
                plainText += newPosition;
                j++;
            }
        }



        for (int i = 0; i < cipherTextRemainKey; i++) {

            keyToChar[i] = keyVal.charAt(i);
            int keyPos = alphabet.indexOf(keyToChar[i]);
            int charPos = alphabet.indexOf(cipherText.charAt(j));  //indexOf shomare un char ro to string mide
            int numberOfShift = (keyPos + charPos) % 64;
            char newPosition = alphabet.charAt(numberOfShift);
            plainText += newPosition;
            j++;
        }
        return plainText;
    }

}
