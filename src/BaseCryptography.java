import sun.security.util.BitArray;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

import static jdk.nashorn.internal.parser.TokenType.OR;

/**
 * Created by Soudabeh on 3/24/2017.
 */
public  abstract class BaseCryptography {

    //method ha va motaghayer haye moshtarak inja


    public String keyVal ;  // kelid baray ramzgozari sade o pichide hast
    public static final String alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /**
     * method encrypt baraye ramzgozari hast ham sade ham pichide
     * @param plainText yek text ra migirad va un ra ramzgizari mikonad
     * @return hasele matne ramzgozari shode ra be soorate string bar migardanad.
     */
    public abstract String encrypt(String plainText);

    /**
     * method decrypt baraye ramz goshayi hast ham sade ham pichide
     * @param cipherText yek text ra migirad va un ra ramzgoshayi mikonad
     * @return asele matne ramzgoshayi shode ra be soorate string bar migardanad.
     */
    public abstract String decrypt(String cipherText);




}
