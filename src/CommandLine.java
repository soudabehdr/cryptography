import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by Soudabeh on 3/31/2017.
 */
public class CommandLine {
    public static void main(String[] args) throws IOException {
        HashMap<String, Integer> keyWords = new HashMap();
        keyWords.put("-es", 1);
        keyWords.put("-ds", 1);
        keyWords.put("-ec", 1);
        keyWords.put("-dc", 1);
        keyWords.put("-i", 1);
        keyWords.put("-o", 1);
        keyWords.put("r", 0);

        for (int i1 = 0; i1 < args.length; i1++) {
            int x = i1;
            if (keyWords.get(args[i1]) != null) {
                for (int j = 0; j < keyWords.get(args[x]); j++) {
                    i1++;
                    if (i1 >= args.length) {
                        System.out.println("No Argument for Command : " + args[x]);
                        System.exit(0);
                    }

                    if (keyWords.get(args[i1]) != null) {
                        System.out.println("No Argument for Command : " + args[x]);
                        System.exit(0);
                    }
                }
            } else {
                System.out.println("Incorrect Input");
                System.exit(0);
            }
        }

            int flag = 0;
            for (int i = 0; i < args.length; i++) {
                if (args[i].startsWith("-")) {
                    flag = 1;
                    switch (args[i]) {

                        case "-es":
                            SimpleShiftCryptography simpleShiftCryptography = new SimpleShiftCryptography(args[i + 1]);
                            String input = "";
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-i")) {
                                    input = args[a + 1];
                                }
                            }
//                        if(input.equals("")){
//                            System.out.println("file vorodi vojod nadarad!");
//                        }
                            InputFileReader inputFileReader = new InputFileReader();
                            byte[] arrayOfByte = InputFileReader.readFile(input);
                            String str = new String(arrayOfByte);
                            String fileEncrypt = simpleShiftCryptography.encrypt(str);
                            boolean isO = true;
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-o")) {
                                    isO = false;
                                    //  File file= new File(args[a+1]); // in edamasho nemidonam chjoori bnevisaaam
                                    //   file.createNewFile();
                                    OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                    outPutFileWriter.writeFile(args[a + 1], fileEncrypt); // args[i+1] directory hast va file nist???
                                    break;
                                }
                            }
                            if (!isO) {
                                OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                outPutFileWriter.writeFile(input, fileEncrypt); // args[i+1] directory hast va file nist???
                                break;
                            }


                            break;
                        case "-ds":
                            SimpleShiftCryptography simpleShiftCryptography1 = new SimpleShiftCryptography(args[i + 1]);
                            String input1 = "";
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-i")) {
                                    input1 = args[a + 1]; // in address file mishe
                                    break;

                                }
                            }
//                        if(input1.equals("")){
//                            System.out.println("file vorodi vojod nadarad!");
//                        }
                            InputFileReader inputFileReader1 = new InputFileReader();
                            byte[] arrayOfByte1 = InputFileReader.readFile(input1);
                            String str1 = new String(arrayOfByte1);
                            String fileDecrypt = simpleShiftCryptography1.decrypt(str1);
                            boolean isO1 = true;
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-o")) {
                                    isO1 = false;
                                    OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                    outPutFileWriter.writeFile(args[a + 1], fileDecrypt); // args[i+1] directory hast va file nist???
                                }
                            }
                            if (!isO1) {
                                OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                outPutFileWriter.writeFile(input1, fileDecrypt); // args[i+1] directory hast va file nist???
                                break;
                            }
                            break;
                        case "-ec":
                            ComplexShiftCryptography complexShiftCryptography = new ComplexShiftCryptography(args[i + 1]);
                            String input2 = "";
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-i")) {
                                    input2 = args[a + 1]; // in address file mishe
                                    break;

                                }
                            }
//                        if(input2.equals("")){
//                            System.out.println("file vorodi vojod nadarad!");
//                        }
                            InputFileReader inputFileReader2 = new InputFileReader();
                            byte[] arrayOfByte2 = InputFileReader.readFile(input2);
                            String str2 = new String(arrayOfByte2);
                            String fileEncrypt1 = complexShiftCryptography.encrypt(str2);
                            boolean isO2 = true;
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-o")) {
                                    isO2 = true;
                                    OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                    outPutFileWriter.writeFile(args[a + 1], fileEncrypt1); // args[i+1] directory hast va file nist???
                                }
                            }
                            if (isO2 == false) {
                                OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                outPutFileWriter.writeFile(input2, fileEncrypt1); // args[i+1] directory hast va file nist???
                                break;
                            }
                            break;
                        case "-dc":
                            ComplexShiftCryptography complexShiftCryptography1 = new ComplexShiftCryptography(args[i + 1]);
                            String input3 = "";
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-i")) {
                                    input3 = args[a + 1]; // in address file mishe
                                    break;

                                }
                            }
//                        if(input3.equals("")){
//                            System.out.println("file vorodi vojod nadarad!");
//                        }
                            InputFileReader inputFileReader3 = new InputFileReader();
                            byte[] arrayOfByte3 = InputFileReader.readFile(input3);
                            String str3 = new String(arrayOfByte3);
                            String fileDecrypt1 = complexShiftCryptography1.decrypt(str3);
                            boolean isO3 = true;
                            for (int a = 0; a < args.length; a++) {
                                if (args[a].equals("-o")) {
                                    isO3 = true;
                                    OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                    outPutFileWriter.writeFile(args[a + 1], fileDecrypt1); // args[i+1] directory hast va file nist???
                                }
                            }
                            if (!isO3) {
                                OutPutFileWriter outPutFileWriter = new OutPutFileWriter();
                                outPutFileWriter.writeFile(input3, fileDecrypt1); // args[i+1] directory hast va file nist???
                                break;
                            }
                            break;
                   /* case "i": // address filei k bayad az roosh bkhone

                        String filekhandeShode=args[i+1];
                        InputFileReader inputFileReader4= new InputFileReader();
                        byte[] arrayOfByte4 = InputFileReader.readFile(filekhandeShode);
                        String str4= new String(arrayOfByte4);



                        break;
                    case "o":
                        File fileOut = new File(args[i+1]);
                        break;*/

//                    default:
//                        System.out.println("commandLine isnt correct, pleaze try again!");

                    }
                }
//            if (flag == 0)
//                System.out.println("commandLLine most be start with - ");

            }


            for (int j = 0; j < args.length; j++) {
                String inputr = "";
                if (args[j].equals("-r")) {
                    for (int a = 0; a < args.length; a++) {
                        if (args[a].equals("-i")) {
                            inputr = args[a + 1];
                        }
                        File fileRemove = new File(inputr);
                        fileRemove.delete();
                    }
                }
            }
        }

    }
