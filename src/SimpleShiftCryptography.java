/**
 * Created by Soudabeh on 3/24/2017.
 */
public class  SimpleShiftCryptography  extends BaseCryptography{



    /**
     * SimpleShiftCryptography in sazande baraye gereftane meghdar hast ke chon field keyVal dar class pedar hast
     * pas baraye inke vaghti az vorodi migirad ba ramzgozari pichide motamayez shavad yek field String dar class pedar
     * gerefte va inja dar sazande set mikonim.
     * @param keyString gerefte va ba keyval mojod dar class set mikonad.
     */

    public SimpleShiftCryptography(String keyString){
        keyVal= keyString;

    }

    public SimpleShiftCryptography(){

    }


    /**
     * encrypt baraye ramzgizari sade hast. yek text ra gerefte va bar asase keyVal ke yek string hast unra ramzgozari
     * mikonad intori ke aval keyVal ra pars karde be int sepas character be character  text ra be un meghdar be chap
     * shift midahad.
     * @param plainText yek text ra migirad va un ra ramzgizari mikonad
     * @return natije matne ramzgozari shode ra be soorate string bar migardanad.
     */
    public  String encrypt(String plainText){

      //  plainText=plainText.toUpperCase();   ino nmidonm sorate soal mikkhad ya mesale
        String cipherText="";

        for(int i=0;i<plainText.length();i++){

            int charPos=alphabet.indexOf(plainText.charAt(i));  //indexOf shomare un char ro to string mide

            if(charPos<Integer.parseInt(keyVal)) {
                int numberOfShift = 64-Integer.parseInt(keyVal)+charPos;
                char newPosition = alphabet.charAt(numberOfShift);
                cipherText += newPosition;
            }
            if(charPos>=Integer.parseInt(keyVal)){
                int numberOfShift = charPos-Integer.parseInt(keyVal);
                char newPosition = alphabet.charAt(numberOfShift);
                cipherText += newPosition;
            }
        }
        return cipherText;
    }


//----------------------------------------------------------------------------------------------------------------------
    /**
     * decrypt yek matn ra gerefte va un ra ramz goshayi mokonad. intor ke yek KeyVal az noe string migirad va aval unra
     * be int pars karde va be mizane keyVal character be character be rast shift midahad.
     * @param cipherText yek text ra migirad va un ra ramzgoshayi mikonad
     * @return matn ramzgoshayi shode ra be soorate String baz migardanad.
     */

    public  String decrypt(String cipherText){

      //  cipherText=cipherText.toUpperCase();   // soorate soal gofte horofe bozorg
        String plainText ="";

        for(int i=0;i<cipherText.length();i++){

            int charPos=alphabet.indexOf(cipherText.charAt(i));  //indexOf shomare un char ro to string mide
            int numberOfShift = (Integer.parseInt(keyVal) + charPos) % 64;
            char newPosition = alphabet.charAt(numberOfShift);
            plainText += newPosition;
        }
        return plainText;
    }

}
